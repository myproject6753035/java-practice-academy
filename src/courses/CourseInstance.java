package courses;

import people.*;
import validator.Validator;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class CourseInstance {
    private Course course;
    private TechnicalTrainer technicalTrainer;
    private LocalDate startDate;
    private LocalDate endDate;
    private List<Student> students;

    public CourseInstance(Course course, TechnicalTrainer technicalTrainer, LocalDate startDate) {
        this.setCourse(course);
        this.setTechnicalTrainer(technicalTrainer);
        this.setStartDate(startDate);
        this.calculateEndDate();
        this.students = new ArrayList<Student>();
    }

    public Collection<Student> getStudents() {
        return Collections.unmodifiableCollection(this.students);
    }

    public Student getStudentById(int id) {
        for (Student s : this.students) {
            if (s.getId() == id) {
                return s;
            }
        }

        throw new IllegalArgumentException("Invalid argument! Argument given: " + id);
    }

    public void joinStudent(Student student) {
        this.students.add(student);
    }

    public Course getCourse() {
        return this.course;
    }

    public TechnicalTrainer getTechnicalTrainer() {
        return this.technicalTrainer;
    }

    public LocalDate getStartDate() {
        return this.startDate;
    }

    public LocalDate getEndDate() {
        return this.endDate;
    }

    private void setCourse(Course course) {
        this.course = course;
    }

    private void setTechnicalTrainer(TechnicalTrainer technicalTrainer) {
        this.technicalTrainer = technicalTrainer;
    }

    private void setStartDate(LocalDate startDate) {
        boolean isDateInFuture = Validator.isDateInFuture(startDate);

        if (!isDateInFuture) {
            String exceptionMessage = String.format("Invalid argument! Argument given: %s", startDate);
            throw new IllegalArgumentException(exceptionMessage);
        }

        this.startDate = startDate;
    }

    private void calculateEndDate() {
        byte durationWeeks = this.course.getDurationWeeks();
        this.endDate = this.startDate.plusDays(durationWeeks * 7);
    }
}
