package courses;

import validator.Validator;

public class Course {
    private static final int PAYMENT_PER_WEEK = 40;

    private String name;
    private byte durationWeeks;
    private double price;

    public Course(String name, byte durationWeeks) {
        this.setName(name);
        this.setDurationWeeks(durationWeeks);
        this.calculatePrice();
    }

    public String getName() {
        return this.name;
    }

    public byte getDurationWeeks() {
        return this.durationWeeks;
    }

    public double getPrice() {
        return this.price;
    }

    private void setName(String name) {
        this.name = Validator.validateName(name);
    }

    private void setDurationWeeks(byte durationWeeks) {
        this.durationWeeks = Validator.validateDuration(durationWeeks);
    }

    private void calculatePrice() {
        this.price = this.durationWeeks * PAYMENT_PER_WEEK;
    }
}
