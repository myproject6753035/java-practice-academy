import courses.*;
import people.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.Month;
import java.util.Collection;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        testAcademy();

        // Test all methods and classes of this demo project!
        // Feel free to add more functionalities!
    }

    private static void testAcademy() throws FileNotFoundException {
        Academy academy = new Academy("Soft Academy");
        TechnicalTrainer trainer = new TechnicalTrainer("Kiril", "Ivanov", (byte)25);
        academy.hireEmployee(trainer);

        Course course = new Course("Java", (byte)10);
        CourseInstance courseInstance = new CourseInstance(course, trainer, LocalDate.of(2025, Month.DECEMBER, 20));
        academy.addCourseInstance(courseInstance);

        File studentsFile = new File("input\\students.txt");
        Scanner scanner = new Scanner(studentsFile);

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] tokens = line.split(", ");

            String firstName = tokens[0];
            String lastName = tokens[1];
            byte age = Byte.parseByte(tokens[2]);

            AdultStudent student = new AdultStudent(firstName, lastName, age);
            courseInstance.joinStudent(student);
        }

        Collection<Student> students = courseInstance.getStudents();

        for (Student s : students) {
            System.out.println(s);
        }
    }
}
