package validator;

import java.time.LocalDate;

public class Validator {
    private static final int MIN_SALARY = 640;
    private static final int MAX_SALARY = 3600;
    private static final int MIN_DURATION_WEEKS = 2;
    private static final int MAX_DURATION_WEEKS = 16;
    private static final int MIN_PERSON_AGE = 16;
    private static final int MAX_PERSON_AGE = 65;
    private static final int MIN_CHILD_AGE = 6;

    public static String validateName(String name) {
        boolean isEmpty = name == null || name.isEmpty();

        if (isEmpty) {
            throw new IllegalArgumentException("Name cannot be null or empty string!");
        }

        boolean startsWithCapital = 65 <= name.charAt(0) && name.charAt(0) <= 90;

        if (!startsWithCapital) {
            throw new IllegalArgumentException("Name should start with capital letter!");
        }

        boolean containsDigit = false;

        for (char symbol : name.toCharArray()) {
            if (48 <= symbol && symbol <= 57) {
                containsDigit = true;
                break;
            }
        }

        if (containsDigit) {
            throw new IllegalArgumentException("Name cannot contain digits!");
        }

        return name.trim();
    }

    public static byte validatePersonAge(byte age) {
        if (age < MIN_PERSON_AGE || MAX_PERSON_AGE < age) {
            throw new IllegalArgumentException("Invalid person age!");
        }

        return age;
    }

    public static byte validateChildAge(byte age) {
        if (age < MIN_CHILD_AGE || MIN_PERSON_AGE <= age) {
            throw new IllegalArgumentException("Invalid child age!");
        }

        return age;
    }

    public static boolean isSalaryValid(double salary) {
        return MIN_SALARY <= salary && salary <= MAX_SALARY;
    }

    public static byte validateDuration(byte durationWeeks) {
        if (durationWeeks < MIN_DURATION_WEEKS || MAX_DURATION_WEEKS < durationWeeks) {
            throw new IllegalArgumentException("Invalid duration!");
        }

        return durationWeeks;
    }

    public static boolean isDateInFuture(LocalDate date) {
        return date.isAfter(LocalDate.now());
    }
}
