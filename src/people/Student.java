package people;

public abstract class Student extends Person {
    private byte certificatesCount;

    public Student(String firstName, String lastName, byte age) {
        super(firstName, lastName, age);
        this.certificatesCount = 0;
    }

    public void receiveCertificate() {
        this.certificatesCount++;
    }

    public int getCertificatesCount() {
        return this.certificatesCount;
    }
}
