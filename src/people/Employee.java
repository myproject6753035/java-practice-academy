package people;

import validator.Validator;

public abstract class Employee extends Person {
    private static final double DEFAULT_SALARY = 1200d;
    private static final int TAXES_PERCENT = 40;
    private double salary;

    public Employee(String firstName, String lastName, byte age) {
        super(firstName, lastName, age);
        this.salary = DEFAULT_SALARY;
    }

    public double getSalary() {
        return this.salary;
    }

    public void receiveSalary(double salaryAmount) {
        boolean isSalaryValid = Validator.isSalaryValid(salaryAmount);

        if (!isSalaryValid) {
            String exceptionMessage = String.format("Invalid argument! Argument given: %s", salaryAmount);
            throw new IllegalArgumentException(exceptionMessage);
        }

        double amountAfterTaxes = salaryAmount * (100 - TAXES_PERCENT) / 100;
        this.salary += amountAfterTaxes;
    }
}
