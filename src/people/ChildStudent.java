package people;

import validator.Validator;

public class ChildStudent extends Student {
    public ChildStudent(String firstName, String lastName, byte age) {
        super(firstName, lastName, age);
    }

    // TODO: add more functionalities

    @Override
    public void setAge(byte age) {
        this.age = Validator.validateChildAge(age);
    }
}
