package people;

import validator.*;

public abstract class Person implements Identifiable {
    private static long idCounter = 1;
    private long id;
    private String firstName;
    private String lastName;
    protected byte age;

    public Person(String firstName, String lastName, byte age) {
        this.generateId();
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setAge(age);
    }

    @Override
    public long getId() {
        return this.id;
    }

    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }

    public byte getAge() {
        return this.age;
    }

    public void setAge(byte age) {
        this.age = Validator.validatePersonAge(age);
    }

    private void setFirstName(String firstName) {
        this.firstName = Validator.validateName(firstName);
    }

    private void setLastName(String lastName) {
        this.lastName = Validator.validateName(lastName);
    }

    private void generateId() {
        this.id = idCounter;
        idCounter++;
    }

    @Override
    public String toString() {
        return this.getFullName();
    }
}
