package people;

public interface Identifiable {
    long getId();
}
