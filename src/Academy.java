import courses.CourseInstance;
import people.Employee;
import people.Student;
import validator.Validator;

import java.util.ArrayList;
import java.util.List;

public class Academy {
    private String name;
    private List<Employee> employees;
    private List<CourseInstance> coursesInstances;

    public Academy(String name) {
        this.setName(name);
        this.employees = new ArrayList<Employee>();
        this.coursesInstances = new ArrayList<CourseInstance>();
    }

    public String getName() {
        return this.name;
    }

    public void paySalary(Employee employee, double salaryAmount) {
        Employee employeeToReceiveSalary = null;

        for (Employee emp : this.employees) {
            if (emp.getId() == employee.getId()) {
                employeeToReceiveSalary = emp;
            }
        }

        if (employeeToReceiveSalary == null) {
            String exceptionMessage = String.format("Employee with ID = %d does not exist!", employee.getId());
            throw new IllegalArgumentException(exceptionMessage);
        }

        employeeToReceiveSalary.receiveSalary(salaryAmount);
    }

    public void giveCertificate(Student student) {
        student.receiveCertificate();
    }

    public void hireEmployee(Employee employee) {
        this.employees.add(employee);
    }

    public void addCourseInstance(CourseInstance courseInstance) {
        this.coursesInstances.add(courseInstance);
    }

    private void setName(String name) {
        this.name = Validator.validateName(name);
    }
}
